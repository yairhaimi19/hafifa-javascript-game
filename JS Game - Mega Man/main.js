import { Display } from "./display.js";
import { Controller } from "./controller.js";
import { Engine } from "./engine.js";
import { Game } from "./game.js";
import { Frame } from "./frame.js";
import { cutmanLevel, fourPlayerLevel } from "./levels.js";

const canvas = document.getElementById('game-canvas');

window.addEventListener("load", (event) => {
    "use strict";
    
    let keyDownUp = function(event) {
        pOneController.keyDownUp(event.type, event.keyCode);
        pTwoController.keyDownUp(event.type, event.keyCode);
        pThreeController.keyDownUp(event.type, event.keyCode);
        pFourController.keyDownUp(event.type, event.keyCode);
    }

    let resize = function(event) {
        display.resize(document.documentElement.clientWidth - 32, document.documentElement.clientHeight - 32, game.world.height / game.world.width);
        display.render();
    };

    let render = function() {
        display.drawMap(game.getMap(), 32, game.getCurrentTileSheet());
        display.drawHealth(playerList, playerOne.getHealthFrame(), game.world.width);
        
        const activeShots = game.world.getActiveShots();
        for (const shot of activeShots) {
            display.drawShot(shot.getSpriteSheet(), shot.getFrame(), shot.x, shot.y);
        }
        
        const players = game.world.getPlayers();
        for (const player of players) {
            if (player.isDead()) {
                
            } else if (player.isInvulnerable()) {
                if (player.isVisible()) {
                    display.drawPlayer(player.spriteSheet, player.getCurrFrame(), player.x, player.y, player.direction);
                    player.setVisible(false);
                } else {
                    player.setVisible(true);
                }
            } else {
                display.drawPlayer(player.spriteSheet, player.getCurrFrame(), player.x, player.y, player.direction);
            }
        }

        display.render();
    }

    let update = function() {
        for (const player of game.world.getPlayers()) {
            player.checkInputs();
        }

        game.update();
    };

    const frames = {
        "idle"          : [new Frame(  0, 0, 21, 24, 0,  0)],
        "jump"          : [new Frame(129, 0, 26, 30,-2, -6)],
        "run"           : [new Frame( 41, 2, 24, 22, 0,  1), new Frame(65, 0, 16, 24, 0, 1), new Frame(81, 2, 21, 22, 0, 1)],
        "slide"         : [new Frame(102, 3, 27, 21, 0,  2)],
        "climb"         : [new Frame(155, 0, 16, 29, 0,  5)],
        "exit-ladder"   : [new Frame(172, 2, 16, 22)],
        "shoot"         : [new Frame(188, 0, 31, 24)],
        "run-shoot"     : [new Frame(219, 2, 29, 22,-4,  1), new Frame(248, 0, 26, 24, -2, 0), new Frame(274, 2, 30, 22, 0, 1)],
        "jump-shoot"    : [new Frame(304, 0, 29, 30,-4, -6)],
        "knockback"     : [new Frame(357, 1, 28, 22)],
        "health"        : [new Frame(386, 6, 12, 13)]
    };

    let   pOneController    = new Controller(37, 38, 39, 40, 35); // left up right down end
    let   pTwoController    = new Controller(81, 50, 69, 87, 49); // q 2 e w 1
    let   pThreeController  = new Controller(86, 71, 78, 66, 72); // v g n b h
    let   pFourController   = new Controller(73, 57, 80, 79, 48); // i 9 p o 0

    const playerOne   = new Game.World.Player(1, 0, 200, pOneController, frames, "./Sprites/player one.png");
    playerOne.setAnimator(new Display.Animator(playerOne.frames["idle"]));
    playerOne.spawn();
    
    const playerTwo   = new Game.World.Player(2, 100, 0, pTwoController, frames, "./Sprites/player two.png");
    playerTwo.setAnimator(new Display.Animator(playerTwo.frames["idle"]));
    playerTwo.spawn();
    
    const playerThree = new Game.World.Player(3, 410, 0, pThreeController, frames, "./Sprites/player three.png");
    playerThree.setAnimator(new Display.Animator(playerThree.frames["idle"]));
    playerThree.spawn();
    
    const playerFour  = new Game.World.Player(4, 480, 200, pFourController, frames, "./Sprites/player four.png");
    playerFour.setAnimator(new Display.Animator(playerFour.frames["idle"]));
    playerFour.spawn();

    const playerList = [playerOne, playerTwo, playerThree, playerFour];

    let display         = new Display(canvas);
    let game            = new Game(fourPlayerLevel, playerList);
    let engine          = new Engine(1000/30, render, update);
    
    display.buffer.canvas.height = game.world.height;
    display.buffer.canvas.width = game.world.width;

    fourPlayerLevel.setTileSheet(16, 9, () => {
        resize();
        engine.start();
    })

    window.addEventListener("keydown", keyDownUp);
    window.addEventListener("keyup", keyDownUp);
    window.addEventListener("resize", resize);
})
