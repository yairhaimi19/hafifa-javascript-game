export const Controller = function(leftKeyCode, jumpKeyCode, rightKeyCode, slideKeyCode, shootKeyCode) {
    this.left   = new Controller.ButtonInput();
    this.right  = new Controller.ButtonInput();
    this.jump   = new Controller.ButtonInput();
    this.slide  = new Controller.ButtonInput();
    this.shoot  = new Controller.ButtonInput();
    this.leftKeyCode = leftKeyCode;
    this.jumpKeyCode = jumpKeyCode;
    this.rightKeyCode = rightKeyCode;
    this.slideKeyCode = slideKeyCode;
    this.shootKeyCode = shootKeyCode;
    
    this.keyDownUp = function(type, keyCode) {
        let isDown = (type == "keydown");

        switch(keyCode) {
            case this.leftKeyCode:
                this.left.getInput(isDown);
                break;
            case this.jumpKeyCode:
                this.jump.getInput(isDown);
                break;
            case this.rightKeyCode:
                this.right.getInput(isDown);
                break;
            case this.slideKeyCode:
                this.slide.getInput(isDown);
                break;
            case this.shootKeyCode:
                this.shoot.getInput(isDown);
                break;
        }
    }
}

Controller.prototype = {
    constructor: Controller,

    isLeftPressed() {
        return this.left.active;
    },
    
    isRightPressed() {
        return this.right.active;
    },
    
    isJumpPressed() {
        return this.jump.active;
    },
    
    isSlidePressed() {
        return this.slide.active;
    },
    
    isShootPressed() {
        return this.shoot.active;
    },

    disableJump() {
        this.jump.active = false;
    },

    disableShoot() {
        this.shoot.active = false;
    }
    
}

Controller.ButtonInput = function() {
    this.active = false;
    this.down = false;
}

Controller.ButtonInput.prototype = {
    constructor: Controller.ButtonInput,
    getInput: function(isKeyDown) {
        if (this.down != isKeyDown) {
            this.down = isKeyDown;
            this.active = isKeyDown;
        }
    }
}