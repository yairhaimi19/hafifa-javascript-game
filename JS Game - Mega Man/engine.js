export const Engine = function(timeBetweenRefresh, update, render) {
    this.timeSinceRefresh                   = 0;                    // Time since the engine refresh
    this.animationFrameRequest              = undefined;    
    this.lastRefreshStamp                   = undefined;            // The most recent timestamp of the engine refresh
    this.timeBetweenRefresh                 = timeBetweenRefresh;   // In milliseconds (1000/30 is 30 RPS)
    this.wasUpdated = false;   // Whether or not the update function has been called since the last cycle
    this.update = update;
    this.render = render;

    this.run = function(currentTimeStamp) {
        this.timeSinceRefresh += currentTimeStamp - this.lastRefreshStamp;
        this.lastRefreshStamp = currentTimeStamp;

        /* 
        If the client device is too slow, updates may take longer than the time step.
        If this is the case, it could freeze the game and overload the CPU. To prevent this,
        we catch the lag snowball early and never allow three full frames to pass without an update. 
        This is not ideal, but at least the user won't crash their cpu. 
        */
        if (this.timeSinceRefresh >= this.timeBetweenRefresh * 3) {
            this.timeSinceRefresh = this.timeBetweenRefresh;
        }

        /* 
        Since we can only update when the screen is ready to draw and requestAnimationFrame
        calls the run function, we need to keep track of how much time has passed. We
        store that accumulated time and test to see if enough has passed to justify
        an update. Remember, we want to update every time we have accumulated one time step's
        worth of time, and if multiple time steps have accumulated, we must update one
        time for each of them to stay up to speed.
        */
        while (this.timeSinceRefresh >= this.timeBetweenRefresh) {
            this.timeSinceRefresh -= this.timeBetweenRefresh;
            this.update(currentTimeStamp);
            this.wasUpdated = true;
        }

        if (this.wasUpdated) {
            this.wasUpdated = false;
            this.render(currentTimeStamp);
        }

        this.animationFrameRequest = window.requestAnimationFrame(this.handleRun);
    }

    this.handleRun = (timeBetweenRefresh) => { this.run(timeBetweenRefresh); };
}

Engine.prototype = {
    constructor: Engine,
    start: function() {
        this.timeSinceRefresh = this.timeBetweenRefresh;
        this.lastRefreshStamp = window.performance.now();
        this.animationFrameRequest = window.requestAnimationFrame(this.handleRun);
    },
    stop: function() {
        window.cancelAnimationFrame(this.animationFrameRequest);
    }
}