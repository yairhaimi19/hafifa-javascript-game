import {Level} from './level.js';

const collisionTypes = {
    PASS:       0,
    BLOCK:      1,
    LADDER:     2,
    PLATFORM:   3,
    SPIKE:      4
}

const assignCollisionToTile = function(tileNum, collisionType, collisionAssignmentArray) {
    collisionAssignmentArray[tileNum] = collisionType;
}

const cutmanTilePlacement = 
        [ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
          9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
          9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
          9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
          9, 9, 9, 9, 9, 9, 9, 9,25,26, 9, 9, 9, 9, 9, 9,
          9, 9, 9, 9, 9, 9, 9, 9,34,35, 9, 9, 9, 9, 9, 9,
          14,14,14,14,14,10,14,14,14,14,10,14, 9, 9,14,14,
          12,12,12,12,12,10,12,12, 9, 9,10, 9, 9, 9, 9, 9,
          15,15,15,13,13,10, 7,13, 9, 9,25,26, 9, 9, 9, 9,
          16,16,16,13,13,10,13,13, 9, 9,34,35, 9, 9, 9, 9,
          7, 13, 1,13,13,10,13,13,25,26,25,26, 9, 9, 9, 9,
          13,13,13,13,13,10,13, 7,34,35,34,35, 9, 9, 9, 9,
          14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
          3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4,
          3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4 ];

let cutmanCollisionAssignment = new Array(32).fill(collisionTypes.PASS);
assignCollisionToTile(3, collisionTypes.BLOCK, cutmanCollisionAssignment);
assignCollisionToTile(4, collisionTypes.BLOCK, cutmanCollisionAssignment);
assignCollisionToTile(25, collisionTypes.BLOCK, cutmanCollisionAssignment);
assignCollisionToTile(26, collisionTypes.BLOCK, cutmanCollisionAssignment);
assignCollisionToTile(34, collisionTypes.BLOCK, cutmanCollisionAssignment);
assignCollisionToTile(35, collisionTypes.BLOCK, cutmanCollisionAssignment);
assignCollisionToTile(14, collisionTypes.BLOCK, cutmanCollisionAssignment);
assignCollisionToTile(10, collisionTypes.LADDER, cutmanCollisionAssignment);
assignCollisionToTile(11, collisionTypes.SPIKE, cutmanCollisionAssignment);

let cutmanLevel = new Level(cutmanTilePlacement, cutmanCollisionAssignment, "./Sprites/Levels/Cutman/cutman-level.png");





const fourPlayerMapTilePlacement = 
        [
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 14,14,14,14,14,9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 14,14,14,14,14,14,9, 9, 9, 9, 9,
            9, 9, 9, 9, 9, 9, 9, 9,10, 9, 9, 9, 31,32,32,32,32,32,32,33,9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9,10, 9, 9, 9, 12,12,12,12,12,12,12,12,9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9,10, 9, 9, 9, 13,15,15,13, 7,13,13,13,9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9,10, 9, 9, 9, 13,16,16,13,25,26,13,13,9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 9, 9,10, 9, 9, 9, 1, 13,7, 13,34,35,13, 7,9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 
            14,14,14,14,14,14,14,14,14,9, 9, 9, 14,14,14,14,14,14,14,14,9, 9, 9, 9, 9, 14,14,14,14,14,14,14,
            9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10,9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 10,9, 9,25,26, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,25,26,25,26, 9, 9, 10,9, 9, 9, 9, 
            9, 9, 9, 9, 9, 9, 10,9, 9,34,35, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,34,35,34,35, 9, 9, 10,9, 9, 9, 9, 
            14,14,14,14,14,14,14,14,14,14,14,14,14,11,11,11,11,11,11,14,14,14,14,14,14,14,14,14,14,14,14,14,
            3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 
        ];

let fourPlayerCollisionAssignment = new Array(32).fill(collisionTypes.PASS);
assignCollisionToTile(3, collisionTypes.BLOCK, fourPlayerCollisionAssignment);
assignCollisionToTile(4, collisionTypes.BLOCK, fourPlayerCollisionAssignment);
assignCollisionToTile(25, collisionTypes.BLOCK, fourPlayerCollisionAssignment);
assignCollisionToTile(26, collisionTypes.BLOCK, fourPlayerCollisionAssignment);
assignCollisionToTile(34, collisionTypes.BLOCK, fourPlayerCollisionAssignment);
assignCollisionToTile(35, collisionTypes.BLOCK, fourPlayerCollisionAssignment);
assignCollisionToTile(14, collisionTypes.BLOCK, fourPlayerCollisionAssignment);
assignCollisionToTile(10, collisionTypes.LADDER, fourPlayerCollisionAssignment);
assignCollisionToTile(11, collisionTypes.SPIKE, fourPlayerCollisionAssignment);
let fourPlayerLevel = new Level(fourPlayerMapTilePlacement, fourPlayerCollisionAssignment, "./Sprites/Levels/Cutman/cutman-level.png");

export {cutmanLevel, fourPlayerLevel ,collisionTypes};