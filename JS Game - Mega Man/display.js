export const Display = function(canvas) {
    this.buffer     = document.createElement("canvas").getContext("2d");
    this.context    = canvas.getContext("2d");

    this.drawPlayer = function(spriteSheet, currFrame, xDestination, yDestination, direction) {
        let offset = direction == 1 ? 30 : 0;

        this.buffer.drawImage(spriteSheet.image, 
                              currFrame.xSource, currFrame.ySource + offset,
                              currFrame.width, currFrame.height, 
                              Math.round(xDestination + currFrame.xOffset), Math.round(yDestination + currFrame.yOffset + 8), 
                              currFrame.width, currFrame.height);
        // this.buffer.beginPath();
        // this.buffer.rect(xDestination, yDestination, 13, 32);
        // this.buffer.stroke();
    }

    this.drawShot = function(spriteSheet, shotFrame, xDestination, yDestination) {
        this.buffer.drawImage(spriteSheet.image,
                              shotFrame.xSource, shotFrame.ySource,
                              shotFrame.width, shotFrame.height,
                              Math.round(xDestination), Math.round(yDestination),
                              shotFrame.width, shotFrame.height);
    }

    this.resize = function(width, height, height_width_ratio) {

        if (height / width > height_width_ratio) {
    
          this.context.canvas.height = width * height_width_ratio;
          this.context.canvas.width = width;
    
        } else {
    
          this.context.canvas.height = height;
          this.context.canvas.width = height / height_width_ratio;
    
        }
    
        this.context.imageSmoothingEnabled = false;
    
    };

    this.drawMap = function(map, columns, tileSheet) {
        for (let i = 0; i < map.length; i++) {
            
            const value = map[i];
            const xSource       =           (value % tileSheet.columns) * tileSheet.tileSize;
            const ySource       = Math.floor(value / tileSheet.columns) * tileSheet.tileSize;
            const xDestination  =           (i % columns) * tileSheet.tileSize;
            const yDestination  = Math.floor(i / columns) * tileSheet.tileSize;
      
            this.buffer.drawImage(tileSheet.image, 
                                  xSource, ySource, 
                                  tileSheet.tileSize, tileSheet.tileSize, 
                                  xDestination, yDestination, 
                                  tileSheet.tileSize, tileSheet.tileSize);
        }
    }

    this.drawHealth = function(playerList, healthIconFrame, worldWidth) {
        const spacing                = 5;
        const healthAreaWidth        = 85;
        const spaceBetweenCharacters = 10;
        const xStart = Math.round((worldWidth - (((healthAreaWidth + spaceBetweenCharacters) * playerList.length) - spaceBetweenCharacters)) / 2);
        let xDestination = xStart;
        let yDestination = 5;
        // turn this into variables, not hard numbers
        
        for (const player of playerList) {
            const spriteSheet = player.getSpriteSheet();
            const playerHealth = player.getHealth();
            this.buffer.fillStyle = "rgba(0, 0, 0, 0.75)";
            this.buffer.fillRect(xDestination - 3, yDestination - 1, healthAreaWidth, healthIconFrame.height + 2);

            for (let i = 0; i < playerHealth; i++) {
                this.buffer.drawImage(spriteSheet.image, 
                                      healthIconFrame.xSource, healthIconFrame.ySource, 
                                      healthIconFrame.width, healthIconFrame.height, 
                                      xDestination, yDestination, 
                                      healthIconFrame.width, healthIconFrame.height);
                xDestination += healthIconFrame.width + spacing;
            }

            xDestination += healthAreaWidth - ((healthIconFrame.width + spacing) * playerHealth) + spaceBetweenCharacters;
        }
    }
}

Display.prototype = {
    constructor: Display,
    render: function() {
        this.context.drawImage(this.buffer.canvas, 0, 0, 
                               this.buffer.canvas.width, 
                               this.buffer.canvas.height, 
                               0, 0, 
                               this.context.canvas.width, 
                               this.context.canvas.height);
    }
}

Display.Animator = function(frameSet, delay = 1000/15) {
    this.delay       = delay;
    this.frameSet    = frameSet;
    this.frameIndex  = 0;
    this.timeStampOfLastFrameChange = new Date();
    this.currentTimeStamp = new Date();

    this.animate = function(frameSet) {
        
        if (this.currentTimeStamp - this.timeStampOfLastFrameChange < this.delay) {
            this.currentTimeStamp = new Date();
            let currFrame = this.frameSet[this.frameIndex];
            return currFrame;
        } else {
            this.timeStampOfLastFrameChange = this.currentTimeStamp;
            this.currentTimeStamp = new Date();
            if (this.frameSet === frameSet) {
                this.frameIndex = (this.frameIndex + 1) % frameSet.length;
            } else {
                this.frameIndex = 0;
                this.frameSet = frameSet;
            }
    
            let currFrame = this.frameSet[this.frameIndex];
            return currFrame;
        }
    }
}