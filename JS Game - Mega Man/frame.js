export const Frame = function(xSource, ySource, width, height, xOffset = 0, yOffset = 0) {
    this.xSource = xSource;
    this.ySource = ySource;
    this.width = width;
    this.height = height;
    this.xOffset = xOffset;
    this.yOffset = yOffset;
}