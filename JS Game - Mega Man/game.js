import { collisionTypes } from './levels.js';
import { Frame } from "./frame.js";

const directionsEnum = {
    TOP: 0,
    RIGHT: 1,
    BOTTOM: 2,
    LEFT: 3
}

const areSquaresIntersecting = function(aMinPoint, aMaxPoint, bMinPoint, bMaxPoint) {
    const d1x = bMinPoint.x - aMaxPoint.x;
    const d1y = bMinPoint.y - aMaxPoint.y;
    const d2x = aMinPoint.x - bMaxPoint.x;
    const d2y = aMinPoint.y - bMaxPoint.y;

    if (d2x >= 1 || d2y >= 1) {
        return false;
    }

    if (d1x >= 1 || d1y >= 1) {
        return false;
    }

    return true;
}

const posOfHand = (xPlayer, yPlayer, playerWidth, direction) => {
    return {
        x: xPlayer + playerWidth * (direction == 1 ? 1 : 0),
        y: yPlayer + 16
    };
}

export const Game = function(level, players, callback) {
    this.world = new Game.World(level, players, callback);

    this.update = function() {
        this.world.update();
    }

    this.setLevel = function(level) {
        this.world.level = level;
    }

    this.getMap = function() {
        return this.world.level.getMap();
    }

    this.getCurrentTileSheet = function() {
        return this.world.level.getTileSheet();
    }

    this.setPlayerAnimator = function(animator) {
        this.world.player.setAnimator(animator);
    }
}

Game.World = function(level, players, callback, friction = 0.85, gravity = 2) {
    this.friction   = friction;
    this.gravity    = gravity;
    this.players    = players;
    
    this.columns    = 32;
    this.rows       = 20;
    this.tileSize   = 16;
    this.level      = level;
    this.width      = this.tileSize * this.columns;
    this.height     = this.tileSize * this.rows;
}

Game.World.prototype = {
    collidePlayerWithEnvironment: function(player) {
        if (player.x < 0) {
            player.x = 0;
            player.xVelocity = 0;
        } else if (player.x + player.width > this.width) {
            player.x = this.width - player.width;
        }

        if (player.y < 0) {
            player.y = 0;
            player.yVelocity = 0;
        }

        const tilePlacementArray = this.level.getMap();
        const collisionAssignmentArray = this.level.getCollisionAssignment();
        for (let i = 0; i < tilePlacementArray.length; i++) {
            const tileNum = tilePlacementArray[i];
            
            if (collisionAssignmentArray[tileNum] == collisionTypes.PASS) {
                
            } else {
                const xTile = Math.floor(i % this.columns) * this.tileSize;
                const yTile = Math.floor(i / this.columns) * this.tileSize;
                
                switch (collisionAssignmentArray[tileNum]) {
                    case collisionTypes.BLOCK:
                        if (areSquaresIntersecting(
                                    {x: xTile, y: yTile}, 
                                    {x: xTile + this.tileSize - 1, y: yTile + this.tileSize - 1},
                                    {x: Math.round(player.x), y: Math.round(player.y)},
                                    {x: Math.round(player.x + player.width - 1), y: Math.round(player.y + player.height - 1)})) {
                            const dX = player.x - xTile;
                            const dY = player.y - yTile;
                            if (Math.max(Math.abs(dX), Math.abs(dY)) == Math.abs(dX)) {
                                if (dX <= 0) {
                                    player.x -= player.width + dX;
                                } else {
                                    player.x += player.width - dX;
                                }

                                player.xVelocity = 0;    
                            } else {
                                if (dY <= 0) {
                                    player.jumping = false;
                                    player.y = player.y - player.height + Math.abs(dY);
                                } else {
                                    player.y = yTile + this.tileSize;
                                }

                                player.yVelocity = 0;
                            }
                        }
                        break;
                        
                    case collisionTypes.SPIKE:
                        if (areSquaresIntersecting(
                                {x: xTile, y: yTile}, 
                                {x: xTile + this.tileSize - 1, y: yTile + this.tileSize - 1},
                                {x: Math.round(player.x), y: Math.round(player.y)},
                                {x: Math.round(player.x + player.width - 1), y: Math.round(player.y + player.height - 1)})) {
                            if (player.isInvulnerable()) {
                                
                            } else {
                                player.spawn();
                                player.decreaseHealth();
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    },

    update() {
        for (const player of this.players) {
            player.update();
            player.yVelocity += this.gravity;
            player.xVelocity *= this.friction;
            this.collidePlayerWithEnvironment(player);
            this.collideShotsWithEnvironment(player);
            this.collideShotsWithPlayers();
        }
    },

    objectOutOfBounds(object) {
        if (object.x < 0) {
            return true;
        } else if (object.x + object.width > this.width) {
            return true;
        }

        if (object.y < 0) {
            return true;
        }

        return false;
    },

    collideShotsWithEnvironment(player) {
        const tilePlacementArray       = this.level.getMap();
        const collisionAssignmentArray = this.level.getCollisionAssignment();
        for (let i = 0; i < tilePlacementArray.length; i++) {
            const tileNum = tilePlacementArray[i];
            let activeShots = player.getActiveShots();
            
            if (collisionAssignmentArray[tileNum] == collisionTypes.PASS) {
                
            } else {
                const xTile = Math.floor(i % this.columns) * this.tileSize;
                const yTile = Math.floor(i / this.columns) * this.tileSize;

                switch (collisionAssignmentArray[tileNum]) {
                    case collisionTypes.BLOCK:
                        player.setActiveShots(activeShots.filter(shot => 
                            !areSquaresIntersecting(
                                {x: xTile,                               y: yTile},
                                {x: xTile + this.tileSize - 1,           y: yTile + this.tileSize - 1},
                                {x: Math.round(shot.x),                  y: Math.round(shot.y)},
                                {x: Math.round(shot.x + shot.width - 1), y: Math.round(shot.y + shot.height - 1)}) &&
                            !this.objectOutOfBounds(shot)));
                        break;
                
                    default:
                        break;
                }
            }
        }
    },

    getActiveShots() {
        let allActiveShots = [];

        for (const player of this.players) {
            const playerShots = player.getActiveShots();
            if (playerShots.length !== 0) {
                console.log()
            }
            allActiveShots = allActiveShots.concat(playerShots);
        }

        return allActiveShots;
    },

    getPlayers() {
        return this.players;
    },

    collideShotsWithPlayers() {
        const players = this.getPlayers();

        for (const shootingPlayer of players) {
            const shots = shootingPlayer.getActiveShots();

            for (const shot of shots) {
                for (const hurtPlayer of players) {
                    if (shootingPlayer.getID() === hurtPlayer.getID()) {
                        
                    } else if (hurtPlayer.isInvulnerable()) {
                        
                    } else if (areSquaresIntersecting(
                                    {x: shot.x, y: shot.y}, 
                                    {x: shot.x + shot.width - 1, y: shot.y + shot.height - 1},
                                    {x: Math.round(hurtPlayer.x), y: Math.round(hurtPlayer.y)},
                                    {x: Math.round(hurtPlayer.x + hurtPlayer.width - 1), y: Math.round(hurtPlayer.y + hurtPlayer.height - 1)})) {
                        hurtPlayer.decreaseHealth();
                        hurtPlayer.turnInvulnerable();
                    }
                }
            }
        }
    },
}

Game.World.Player = function(playerID, x, y, controller, frames, spriteSheetSrc, callback) {
    this.playerID            = playerID;
    this.height              = 32;
    this.width               = 13;
    this.direction           = 1;
    this.x                   = x;
    this.y                   = y;
    this.xVelocity           = 0;
    this.yVelocity           = 0;
    this.maxHealth           = 5;
    this.healthPoints        = 5;
    this.score               = 0;
    this.invulnerable        = false;
    this.visible             = true;
    this.invulnerabilityTime = 700;
    this.shooting            = false;
    this.delayBetweenShots   = 300;
    this.timeStampOfLastShot = new Date();
    this.activeShots         = [];
    this.jumping             = true;
    this.jumpPower           = 21;
    this.moveSpeed           = 0.7;
    this.slideSpeed          = 7;
    this.spriteSheet         = new Game.World.Player.SpriteSheet(spriteSheetSrc, callback);
    this.animator            = undefined;
    this.controller          = controller;

    this.frames = frames;

    this.currFrame = this.frames["idle"][0];

    this.setAnimator = function(animator) {
        this.animator = animator;
    }

    this.getCurrFrame = function() {
        return this.currFrame;
    }

    this.getActiveShots = function() {
        return this.activeShots;
    }

    this.setActiveShots = function(activeShots) {
        this.activeShots = activeShots;
    }

    this.checkInputs = function() {
        if (this.controller.isLeftPressed())  { this.moveLeft();  }
        if (this.controller.isRightPressed()) { this.moveRight(); }
        if (this.controller.isSlidePressed()) { this.slide(); }
        if (this.controller.isJumpPressed())  { this.jump(); this.controller.disableJump() }
        if (this.controller.isShootPressed()) { this.shoot(); this.controller.disableShoot() }
    }

    this.getHealth = function() {
        return this.healthPoints;
    }

    this.decreaseHealth = function() {
        if (this.healthPoints > 0) { 
            this.healthPoints--; 
        }
    }

    this.getHealthFrame = function() {
        return this.frames["health"][0];
    }

    this.getSpriteSheet = function() {
        return this.spriteSheet;
    }

    this.spawn = function() {
        this.x = x;
        this.y = y;
        this.turnInvulnerable();
    },

    this.isInvulnerable = function() {
        return this.invulnerable;
    },

    this.turnInvulnerable = function() {
        this.invulnerable = true;

        setTimeout(() => {
            this.invulnerable = false;
        }, this.invulnerabilityTime)
    },

    this.isVisible = function() {
        return this.visible;
    },

    this.setVisible = function(isVisible) {
        this.visible = isVisible;
    },

    this.isDead = function() {
        if (this.healthPoints == 0) {
            return true;
        }

        return false;
    },

    this.increaseScore = function(amount) {
        this.score += amount;
    }
}

Game.World.Player.prototype = {
    constructor: Game.Player,
    jump() {
        if (!this.jumping) {
            this.jumping = true;
            this.yVelocity -= this.jumpPower;
        }
    },

    moveLeft() { this.xVelocity -= this.moveSpeed; },

    moveRight() { this.xVelocity += this.moveSpeed; },

    slide() {
        this.xVelocity = this.direction == 1 ? this.slideSpeed : -this.slideSpeed;
        this.shooting = false;
    },

    shoot() {
        if ((!this.shooting)                                &&
            (Math.abs(this.xVelocity) != this.slideSpeed)   &&
            (new Date().getTime() - this.timeStampOfLastShot >= this.delayBetweenShots)) {
            this.shooting = true;
            const positionOfHand = posOfHand(this.x, this.y, this.currFrame.width, this.direction);
            this.activeShots.push(new Game.World.Player.Shot(positionOfHand.x, positionOfHand.y, this.direction, this.spriteSheet));
        }

        setTimeout(() => {
            this.shooting = false;
        }, this.delayBetweenShots);
    },

    update() {
        this.updateAnimation();
        if (Math.abs(this.yVelocity) < 0.7) {
            this.yVelocity = 0;
        }
        if (Math.abs(this.xVelocity) < 0.7) {
            this.xVelocity = 0;
        }
        this.y += this.yVelocity;
        this.x += this.xVelocity;
        for (const shot of this.activeShots) {
            shot.update();
        }
    },

    updateAnimation() {
        if (this.xVelocity > 0) {
            this.direction = 1;
        } else if (this.xVelocity < 0) {
            this.direction = -1;
        }

        if (this.jumping && this.shooting) {
            this.currFrame = this.animator.animate(this.frames["jump-shoot"]);
            
        } else if (this.shooting && Math.round(this.xVelocity * 10) / 10 != 0) {
            this.currFrame = this.animator.animate(this.frames["run-shoot"]);

        } else if (this.shooting) {
            this.currFrame = this.animator.animate(this.frames["shoot"]);

        } else if (((this.yVelocity != 0) && (this.yVelocity != 2)) || (this.jumping)) {
            this.currFrame = this.animator.animate(this.frames["jump"]);

        } else if (Math.abs(this.xVelocity) == this.slideSpeed) {
            this.currFrame = this.animator.animate(this.frames["slide"]);

        } else if (Math.round(this.xVelocity * 10) / 10 != 0) {
            this.currFrame = this.animator.animate(this.frames["run"]);

        } else {
            this.currFrame = this.animator.animate(this.frames["idle"]);
        }
    },

    getID() {
        return this.playerID;
    },
}

Game.World.Player.SpriteSheet = function(src, callback) {
    this.image = new Image();
    this.image.addEventListener("load", callback, { once: true });
    this.image.src = src;

    this.setImage = (src, callback) => {
        this.image.addEventListener("load", callback, { once: true });
        this.image.src = src;
    }
}

Game.World.Player.Shot = function(x, y, direction, spriteSheet) {
    this.x           = x;
    this.y           = y;
    this.width       = 4;
    this.height      = 4;
    this.velocity    = 10;
    this.direction = direction;
    this.spriteSheet = spriteSheet;
    this.frames = {
        "regular" : [new Frame(419, 11,  4,   4)]
    }

    this.update = () => {
        this.x += this.velocity * direction;
    }

    this.getFrame = () => {
        return this.frames["regular"][0];
    }

    this.getSpriteSheet = () => {
        return this.spriteSheet;
    }
}