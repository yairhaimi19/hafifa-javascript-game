export const Level = function(tilePlacementArray, collisionAssignmentArray, tileSheetSrc) {
    this.tilePlacementArray         = tilePlacementArray;
    this.collisionAssignmentArray   = collisionAssignmentArray;
    this.tileSheetSrc               = tileSheetSrc;
    this.tileSheet                  = undefined;

    this.assignCollisionToTile = function(tileNum, collisionType) {
        this.collisionAssignmentArray[tileNum] = collisionType;
    }

    this.getMap = function() {
        return this.tilePlacementArray;
    }

    this.getTileSheet = function() {
        return this.tileSheet;
    }

    this.getCollisionAssignment = function() {
        return this.collisionAssignmentArray;
    }

    this.setTileSheet = function(tileSize, columns, callback) {
        this.tileSheet = new Level.TileSheet(tileSize, columns);
        this.tileSheet.setImage(tileSheetSrc, callback);
    }
}

Level.TileSheet = function(tileSize, columns) {
    this.image = new Image();
    this.tileSize = tileSize;
    this.columns = columns;

    this.setImage = (src, callback) => {
        this.image.addEventListener("load", callback, { once: true });
        this.image.src = src;
    }
}